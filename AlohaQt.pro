TEMPLATE = app
TARGET = Aloha App

QT = core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

SOURCES += \
    main.cpp \
    ventanita.cpp

HEADERS += \
    ventanita.h
