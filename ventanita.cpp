#include "ventanita.h"
#include <QApplication>
#include <QMessageBox>

#include <iostream>

Ventanita::Ventanita(QWidget *parent) : QWidget(parent)
{
    // Set size of the window
    setFixedSize(600, 50);

    // Boton Quit
    m_buttonQuit  = new QPushButton("Chau!", this);
    m_buttonQuit->setGeometry(110, 10, 80, 30);
    connect(m_buttonQuit, SIGNAL (clicked()), QApplication::instance(), SLOT (quit()));

    // Boton Aloha
    m_buttonAloha = new QPushButton("Aloha!", this);
    m_buttonAloha->setGeometry(10, 10, 80, 30);
    connect(m_buttonAloha, SIGNAL (clicked()), this, SLOT (aloha()));


    // Progress Bar
    m_progressBar = new QProgressBar(this);
    m_progressBar->setRange(0, 100);
    m_progressBar->setValue(0);
    m_progressBar->setGeometry(210, 10, 180, 30);

    // Progress Bar
    m_slider = new QSlider(this);
    m_slider->setOrientation(Qt::Horizontal);
    m_slider->setRange(0, 100);
    m_slider->setValue(0);
    m_slider->setGeometry(410, 15, 180, 20);
    QObject::connect(m_slider, SIGNAL (valueChanged(int)), m_progressBar, SLOT (setValue(int)));
}

void Ventanita::aloha(void)
{
    QMessageBox msgBox;
    msgBox.setText("The document has been modified.");
    msgBox.setInformativeText("Do you want to save your changes?");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Yes);

    int ret = msgBox.exec();
    if (ret == QMessageBox::Yes) {
        std::cout << std::endl << "Yes!" << std::endl;
    } else if (ret == QMessageBox::No) {
        std::cout << std::endl << "No!" << std::endl;
    } else if(ret == QMessageBox::Cancel) {
        std::cout << std::endl << "Cancel!" << std::endl;
    }
}
