#ifndef VENTANITA_H
#define VENTANITA_H

#include <QWidget>
#include <QPushButton>
#include <QProgressBar>
#include <QSlider>

class Ventanita : public QWidget
{
    Q_OBJECT
private:
    QPushButton  *m_buttonQuit;
    QPushButton  *m_buttonAloha;
    QProgressBar *m_progressBar;
    QSlider      *m_slider;

public:
    explicit Ventanita(QWidget *parent = nullptr);

signals:

public slots:
    void aloha(void);

};

#endif // VENTANITA_H
