#include <QApplication>
#include <ventanita.h>

int main(int argc, char **argv)
{
    QApplication app (argc, argv);

    Ventanita v;
    v.show();

    return app.exec();
}
